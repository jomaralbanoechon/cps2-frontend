//window.location.search returns the query string part of the URL
// console.log(window.location.search)

//instantiate a URLSearchParams object so we can access specific parts of the query string
let params = new URLSearchParams(window.location.search)

//get returns the value of the key passed as an argument, then we store it in a variable
let courseId = params.get('courseId')

//get the token from localStorage
let token = localStorage.getItem('token')
let adminUser = localStorage.getItem("isAdmin")

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`https://cps2.herokuapp.com/api/courses/${courseId}`)
  .then((res) => res.json())
  .then((data) => {
    // console.log(data);

    courseName.innerHTML = data.name;
    courseDesc.innerHTML = data.description;
    coursePrice.innerHTML = data.price;

    if(adminUser === "false" || !adminUser){
      enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-secondary"> Enroll </button>`;

      document.querySelector("#enrollButton").addEventListener("click", () => {
      // insert the course to our courses collection
      fetch("https://cps2.herokuapp.com/api/users/enroll", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          courseId: courseId,
        }),
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          //creation of new course successful
          if (data === true) {
            //redirect to courses page
            alert("Thank you for enrolling! See you!");
            window.location.replace("./courses.html");
          } else {
            //error in creating course
            alert("You must log in first.")
            window.location.replace('./login.html')
          }
        });
    });
    }else{
      data.enrollees.map(course => {
        fetch(`https://cps2.herokuapp.com/api/courses/${courseId}`)
        .then(res => res.json())
        .then(data => {      
          //enrolleesContainer.innerHTML += `<button id="enrollButton" class="btn btn-block btn-primary"> ${data._id} ${course.enrolledOn} ${course.userId}</button>`
          enrolleesContainer.innerHTML +=
            `
              <tr>
                <td>${course.userId}</td>
              </tr>
            `            
            })
      })
    }        
  });