let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')

// console.log(courseId)

fetch(`https://cps2.herokuapp.com/api/courses/${courseId}`)
.then((res) => res.json())
.then((data) => {
	// console.log(data)	

	let token = localStorage.getItem('token')	

		fetch('https://cps2.herokuapp.com/api/courses', {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({				
				courseId: courseId,
				name: data.name,
				description: data.description,
				price: data.price,
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				window.location.replace('./courses.html')				
			}else{
				alert("Something went wrong.")
			}
		})
});