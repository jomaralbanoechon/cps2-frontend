//create a file under client/assets/js named addCourse.js

let formSubmit = document.querySelector("#createCourse")

formSubmit.addEventListener("submit", (e) => {

	// prevent the page from reloading upon submitting the form
    e.preventDefault()

    // get the value of #courseName and assign it to courseName
	let courseName = document.querySelector("#courseName").value

    // get the value of #courseDescription and assign it to description
	let description = document.querySelector("#courseDescription").value

    // get the value of #coursePrice and assign it to price
	let price = document.querySelector("#coursePrice").value

    // retrieve the JWT stored in our local storage for auth
    let token = localStorage.getItem('token')

    // insert the course to our courses collection
    fetch('https://cps2.herokuapp.com/api/courses', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            name: courseName,
            description: description,
            price: price
        })
    })
    .then(res => {
        return res.json()
    })
    .then(data => {
        //creation of new course successful
        if(data === true){
            //redirect to courses page
            window.location.replace("./courses.html")
        }else{
            //error in creating course
            alert("Something went wrong")
        }
    })

})